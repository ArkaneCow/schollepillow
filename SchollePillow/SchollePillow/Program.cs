﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace SchollePillow
{
    class Program
    {

        const string FILTER_ALL = "*.*";

        static ArrayList monitorPaths = new ArrayList();
        static ArrayList fsWatchers = new ArrayList();

        static string logString = "";

        static DateTime dateSend = DateTime.Today;
        static TimeSpan sendMailTime = new TimeSpan(12, 00, 00);

        static string destinationEmail = "jasonwupilly1997@gmail.com";
        static string fromEmail = "schollebot@gmail.com";
        static string fromPassword = "fabrice123";

        const string EMAIL_START = "Hello, this is the log for what happened:\n";
        const string EMAIL_END = "\nBye";

        // http://stackoverflow.com/questions/11660235/find-out-usernamewho-modified-file-in-c-sharp
        static string GetSpecificFileProperties(string file, params int[] indexes)
        {
            string fileName = Path.GetFileName(file);
            string folderName = Path.GetDirectoryName(file);
            Type shellAppType = Type.GetTypeFromProgID("Shell.Application");
            Object shell = Activator.CreateInstance(shellAppType);
            Shell32.Folder objFolder;
            objFolder = (Shell32.Folder)shellAppType.InvokeMember("NameSpace", System.Reflection.BindingFlags.InvokeMethod, null, shell, new object[] { folderName });
            StringBuilder sb = new StringBuilder();

            foreach (Shell32.FolderItem2 item in objFolder.Items())
            {
                if (fileName == item.Name)
                {
                    for (int i = 0; i < indexes.Length; i++)
                    {
                        sb.Append(objFolder.GetDetailsOf(item, indexes[i]) + ",");
                    }

                    break;
                }
            }

            string result = sb.ToString().Trim();
            //Protection for no results causing an exception on the `SubString` method
            if (result.Length == 0)
            {
                return string.Empty;
            }
            return result.Substring(0, result.Length - 1);
        }
        
        static void readFilePaths()
        {
            try
            {
                StreamReader sr = new StreamReader("filePaths.txt");
                string filePathContent = sr.ReadToEnd();
                string[] filePaths = filePathContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                for (int fileLineIndex = 0; fileLineIndex < filePaths.Length; fileLineIndex++)
                {
                    string filePath = filePaths[fileLineIndex] + "\\";
                    if (filePath != "\\" && Directory.Exists(filePath))
                    {
                        monitorPaths.Add(filePath);
                        Console.WriteLine("Added " + filePath + " to monitorPaths");
                    }
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        static void createFSWatchers()
        {
            for (int filePathIndex = 0; filePathIndex < monitorPaths.Count; filePathIndex++)
            {
                string filePath = (string) monitorPaths[filePathIndex];
                FileSystemWatcher pathWatcher = new FileSystemWatcher();
                pathWatcher.Path = filePath;
                pathWatcher.Filter = FILTER_ALL;
                pathWatcher.NotifyFilter = NotifyFilters.LastAccess |
                    NotifyFilters.LastWrite |
                    NotifyFilters.FileName |
                    NotifyFilters.DirectoryName;
                pathWatcher.IncludeSubdirectories = true;
                pathWatcher.Changed += new FileSystemEventHandler(FSWOnChanged);
                pathWatcher.Created += new FileSystemEventHandler(FSWOnChanged);
                pathWatcher.Deleted += new FileSystemEventHandler(FSWOnChanged);
                pathWatcher.Renamed += new RenamedEventHandler(FSWOnRenamed);
                pathWatcher.EnableRaisingEvents = true;
                fsWatchers.Add(pathWatcher);
            }
        }

        static void FSWOnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.ToLower().EndsWith(".tmp") || e.Name.StartsWith("~"))
            {
                return;
            }
            logString += "[" + DateTime.Now.ToString() + "] (" + e.ChangeType.ToString() + ") " + e.FullPath + Environment.NewLine + "User " + GetSpecificFileProperties(e.FullPath, 10) + " at " + GetSpecificFileProperties(e.FullPath, 53) + Environment.NewLine;
        }

        static void FSWOnRenamed(object sender, RenamedEventArgs e)
        {
            if (e.FullPath.ToLower().EndsWith(".tmp") || e.OldFullPath.ToLower().EndsWith(".tmp"))
            {
                return;
            }
            logString += "[" + DateTime.Now.ToString() + "] (" + e.ChangeType.ToString() + ") " + e.OldFullPath + " to " + e.FullPath + Environment.NewLine + "User " + GetSpecificFileProperties(e.FullPath, 10) + " at " + GetSpecificFileProperties(e.FullPath, 53) + Environment.NewLine;
        }

        static void sendEmail()
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.Credentials = new NetworkCredential(fromEmail, fromPassword);
            string messageBody = EMAIL_START + logString + EMAIL_END;
            smtp.Send(fromEmail, destinationEmail, "Report for " + DateTime.Now.ToString(), messageBody);
            logString = "";
        }

        static void sendWhenReady()
        {
            while (true)
            {
                TimeSpan currentTime = DateTime.Now.TimeOfDay;
                if (currentTime >= sendMailTime && DateTime.Today == dateSend)
                {
                    sendEmail();
                    dateSend = dateSend.AddDays(1);
                }
            }
        }

        static void startReportThread()
        {
            TimeSpan currentTime = DateTime.Now.TimeOfDay;
            if (currentTime >= sendMailTime && DateTime.Today == dateSend)
            {
                dateSend = dateSend.AddDays(1);
            }
            Thread checkMailThread = new Thread(sendWhenReady);
            checkMailThread.Start();
        }

        static void readConfig()
        {
            try
            {
                StreamReader sr = new StreamReader("config.txt");
                string configContent = sr.ReadToEnd();
                string[] configLines = configContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                for (int configLineIndex = 0; configLineIndex < configLines.Length; configLineIndex++)
                {
                    string configLine = configLines[configLineIndex];
                    if (configLine.StartsWith("destEmail="))
                    {
                        destinationEmail = configLine.Substring(10, configLine.Length - 10);
                    }
                    else if (configLine.StartsWith("fromEmail="))
                    {
                        fromEmail = configLine.Substring(10, configLine.Length - 10);
                    }
                    else if (configLine.StartsWith("fromPass="))
                    {
                        fromPassword = configLine.Substring(9, configLine.Length - 9);
                    }
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        [STAThread]
        static void Main(string[] args)
        {
            readConfig();
            readFilePaths();
            createFSWatchers();
            startReportThread();
        }
    }
}
